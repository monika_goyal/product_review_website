import { TextField } from "@material-ui/core"

const SearchBar = props => {
  
    return (
        <div>
            <TextField
                variant="outlined"
                type="text"
                label="Search Product"
                value={props.search}
                onChange={(e) => props.setSearch(e.target.value)}
            />
        </div>
    )
}

export default SearchBar