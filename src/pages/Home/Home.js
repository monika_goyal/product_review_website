import { useState } from "react"
import ProductListing from "./ProductListing/ProductListing"
import SearchBar from "./SearchBar/SearchBar"

const Home = () => {
    const [search, setSearch] = useState('')

    return(
        <div>
            <SearchBar search={search} setSearch={setSearch} />
            <ProductListing search={search}/>
        </div>
    )
}

export default Home