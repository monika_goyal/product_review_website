import { Card, CardMedia, CardContent, Typography, Grid, TextField, CircularProgress } from "@material-ui/core"
import { getThemeProps } from "@material-ui/styles"
import { useEffect, useState } from "react"
import { withRouter } from "react-router"
import { getProducts } from '../../../services/product-services'
import classes from '../Home.module.css'

const ProductListing = (props) => {
    const [products, setProducts] = useState([])
    const [loader, setLoader] = useState(false)

    useEffect(() => {
        loadProducts()
    }, [])

    const loadProducts = async () => {
        setLoader(true)
        const response = await getProducts()
        setProducts(response.data)
        setLoader(false)
    }


    const onProductSelect = product => {
       props.history.push(`/${product.category}`, product)
    }

    const renderProducts = () => {
        return (
            products.filter(product => { 
                let newSearch = props.search.trim()
                if(!newSearch || newSearch === null){
                    return product
                }else {
                    return product.category.includes(newSearch)
                }
            }).map(product => <Card className={classes["card--container"]} onClick={() => onProductSelect(product)}>
                                    <CardMedia
                                        component="img"
                                        height="140"
                                        image={product.image}
                                        alt="green iguana"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div">
                                        {product.category}
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                        {product.description}
                                        </Typography>
                                        <Typography gutterBottom variant="h5" component="div">
                                        {product.price}
                                        </Typography>
                                    </CardContent>
                                </Card>
            )
        )
    }

    return (

        <Grid xs={6}>
            {   loader ? 
                <CircularProgress />
                : 
                renderProducts()
            }
            
        </Grid>
    )
}

export default withRouter(ProductListing)