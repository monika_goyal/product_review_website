import { Button, TextareaAutosize } from "@material-ui/core"
import { useState } from "react"
import { withRouter } from "react-router"

const ReviewSection = (props) => {
    const [review, setReview] = useState('')

    const onSubmitReview = (e) => {
        e.preventDefault()
        alert("Thank You! Review submitted successfully")
        props.history.push('/')
    }
    return (
        <div>
            <TextareaAutosize
                maxRows={4}
                placeholder="Write your review"
                style={{ width: "60%", height: 200 }}
                value={review}
                onChange={(e) => setReview(e.target.value)}
            />
            <Button 
                variant="contained" 
                color="primary" 
                style={{ display: "block" }}
                type="submit"
                onClick={onSubmitReview}
            >
                Submit Review
            </Button>
        </div>
    )
}

export default withRouter(ReviewSection)