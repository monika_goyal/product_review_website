import { Switch, Route } from "react-router-dom"
import Home from "../pages/Home/Home"
import ReviewSection from "../pages/ReviewSection/ReviewSection"

const Routes = () => {
    return (
        <Switch>
            <Route path='/' component={Home} exact />
            <Route path='/:name' component={ReviewSection} />
        </Switch>
    )
}

export default Routes