import { Toolbar } from "@material-ui/core"

const Footer = () => {
    return (
        <Toolbar className="footer">
            <div>
                <p>Author: Hege Refsnes</p>
                <p><a href="mailto:hege@example.com">hege@example.com</a></p>
            </div>
            <style jsx="true">{`
                .footer{
                    width:100%;
                    position:fixed;
                    bottom:0;
                    background-color:#3f51b5;
                }
                .footer div{
                    width: inherit;
                    text-align: center;
                }
                .footer div p{
                    color: #fff;
                    margin:0;
                }
                .footer div p a{
                    color: #fff;
                }
            `}</style>
        </Toolbar>
    )
}

export default Footer