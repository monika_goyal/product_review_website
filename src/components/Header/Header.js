import { AppBar, Toolbar, Typography, Button } from "@material-ui/core"

const Header = props => {
   
    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" component="div"> Logo </Typography>
            </Toolbar>
        </AppBar>
    )
}

export default Header