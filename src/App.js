import { BrowserRouter } from 'react-router-dom';
import './App.css';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Routes from './routes/Routes';

function App() {
  return (
    <BrowserRouter>
      <Header />
      <div className="container">
        <Routes />
      </div>  
      <Footer />
    </BrowserRouter>
  );
}

export default App;
